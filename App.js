/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import { OTSession, OTPublisher, OTSubscriber } from "opentok-react-native";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);

    this.publisherProperties = {
      // publishAudio: true,
      // cameraPosition: "back"
    };

    this.publisherEventHandlers = {
      streamCreated: event => {
        console.log("Publisher stream created!", event);
      },
      streamDestroyed: event => {
        console.log("Publisher stream destroyed!", event);
      }
    };
  }
  componentDidMount() {
    console.log("moundddted");
  }
  render() {
    return (
      <View style={styles.container}>
        <OTSession
          apiKey="46334862"
          sessionId="1_MX40NjMzNDg2Mn5-MTU1ODk1MDk1Mjg3OH5aMStIRjY2czRxMW1NRSsvMnZHWDhiOFF-QX4"
          token="T1==cGFydG5lcl9pZD00NjMzNDg2MiZzaWc9YmZkZmIxZDU4MjAxYTE2NzgyM2Q1NGM5MTc0MGYwNDVlNjc1MTY3ODpzZXNzaW9uX2lkPTFfTVg0ME5qTXpORGcyTW41LU1UVTFPRGsxTURrMU1qZzNPSDVhTVN0SVJqWTJjelJ4TVcxTlJTc3ZNblpIV0RoaU9GRi1RWDQmY3JlYXRlX3RpbWU9MTU1ODk1MDk4MCZub25jZT0wLjU1MDgyOTk2NTE2NzI4ODgmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTU1OTAzNzM4MCZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ=="
        >
          <OTPublisher
            properties={this.publisherProperties}
            eventHandlers={this.publisherEventHandlers}
            style={{ height: 300, width: 300 }}
          />
          <OTSubscriber
            eventHandlers={this.publisherEventHandlers}
            style={{ width: 100, height: 100 }}
          />
        </OTSession>
        <Text style={styles.welcome}>Welcome to jsssssseff wordddld!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
